const fs = require("fs");
const csvToJson = require("../server.cjs");

const deliveriesPath = "../data/deliveries.csv";
const outputFilePath = "../public/output/8-highest-number-of-times-player-dismissed-by-another-player.json";

function highestNumberOfTimesPlayerDismissedByAnotherPlayer() {
  return csvToJson(deliveriesPath).then((deliveries) => {
    // console.log(deliveries);

    const allDismissal = {};

    for (let delivery of deliveries) {
      const batsman = delivery.player_dismissed;
      const bowler = delivery.bowler;

      if (batsman.trim() === "") {
        continue;
      }

      if (allDismissal[batsman]) {
        if (allDismissal[batsman][bowler]) {
          allDismissal[batsman][bowler] += 1;
        } else {
          allDismissal[batsman][bowler] = 1;
        }
      } else {
        allDismissal[batsman] = { [bowler]: 1 };
      }
    }

    let playerDismissedAnotherPlayerMaxTimeData = {
      player_dismissed: null,
      bowler_name: null,
      count: 0,
    };

    for (const batsman in allDismissal) {
      const bowlerObj = allDismissal[batsman];

      for (const bowler in bowlerObj) {
        const outCount = bowlerObj[bowler];

        if (outCount > playerDismissedAnotherPlayerMaxTimeData.count) {
          playerDismissedAnotherPlayerMaxTimeData = {
            player_dismissed: batsman,
            bowler_name: bowler,
            count: outCount,
          };
        }
      }
    }
    
    fs.writeFileSync(
        outputFilePath,
        JSON.stringify(playerDismissedAnotherPlayerMaxTimeData),
        (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log(
              "highest-number-of-times-player-dismissed-by-another-player.json file is created"
            );
          }
        }
      );

  });
}
console.log(highestNumberOfTimesPlayerDismissedByAnotherPlayer());
