const fs = require("fs");
const csvToJson = require("../server.cjs");

const matchesPath = "../data/matches.csv";
const deliveriesPath = "../data/deliveries.csv";
const outputFilePath = "../public/output/7-strike-rate-of-batsman-for-each-season.json";

// Find the strike rate of a batsman for each season
function strikeRateOfBatsmanForEachSeason() {
  let matchesData;
  let deliveriesData;

  csvToJson(matchesPath)
    .then((matches) => {
      matchesData = matches;

      return csvToJson(deliveriesPath);
    })
    .then((deliveries) => {
      deliveriesData = deliveries;
      const matchesSeason = {};
      for (let index = 0; index < matchesData.length; index++) {
        const match = matchesData[index];
        matchesSeason[match.id] = match.season;
      }

      const playersDetails = {};
      for (let index = 0; index < deliveriesData.length; index++) {
        const deliveries = deliveriesData[index];
        const season = matchesSeason[deliveries.match_id];
        if (!playersDetails[deliveries.batsman]) {
          playersDetails[deliveries.batsman] = {};
        }
        if (!playersDetails[deliveries.batsman][season]) {
          playersDetails[deliveries.batsman][season] = {
            totalRuns: parseInt(deliveries.total_runs),
            totalBalls: 1,
          };
        } else {
          playersDetails[deliveries.batsman][season].totalRuns += parseInt(
            deliveries.total_runs
          );
          playersDetails[deliveries.batsman][season].totalBalls++;
        }
      }
      for (let batsman in playersDetails) {
        for (let season in playersDetails[batsman]) {
          const details = playersDetails[batsman][season];
          // calculate the strike rate
          details.strikeRate = (details.totalRuns / details.totalBalls) * 100;
        }
      }
      fs.writeFileSync(outputFilePath, JSON.stringify(playersDetails), (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log(
            "strike-rate-of-batsman-for-each-season.json file is created"
          );
        }
      });
    });
}
console.log(strikeRateOfBatsmanForEachSeason());
