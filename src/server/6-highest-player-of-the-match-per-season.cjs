const fs = require("fs");
const csvToJson = require("../server.cjs");
const { result } = require("lodash");

const csvFilePath = "../data/matches.csv";
const outputFilePath = "../public/output/6-highest-player-of-the-match-per-season.json";

// Find a player who has won the highest number of Player of the Match awards for each season
function highestPlayerOfTheMatchPerSeason() {
  csvToJson(csvFilePath).then((matches) => {
    // console.log(matches);
    const matchPerSeason = {};
    for (let match of matches) {
      const season = match.season;
      const playerOfMatch = match.player_of_match;
      if (!matchPerSeason[season]) {
        matchPerSeason[season] = {};
      }
      if (!matchPerSeason[season][playerOfMatch]) {
        matchPerSeason[season][playerOfMatch] = 1;
      } else {
        matchPerSeason[season][playerOfMatch]++;
      }
    }
    const highestAwards = [];
    for (let season in matchPerSeason) {
      let highestPlayer;
      let highestCount = 0;
      for (let player in matchPerSeason[season]) {
        if (matchPerSeason[season][player] > highestCount) {
          highestCount = matchPerSeason[season][player];
          highestPlayer = player;
        }
      }
      highestAwards.push({
        season: season,
        player: highestPlayer,
        awards: highestCount,
      });
    }

    fs.writeFileSync(outputFilePath, JSON.stringify(highestAwards), (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("player-of-the-match-by-season.json file is created");
      }
    });
  });
}
console.log(highestPlayerOfTheMatchPerSeason());
