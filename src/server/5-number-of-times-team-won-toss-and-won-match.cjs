const fs = require("fs");
const csvToJson = require("../server.cjs");
const { result } = require("lodash");

const matchesPath = "../data/matches.csv";
const deliveriesPath = "../data/deliveries.csv";
const outputFilePath = "../public/output/5-number-of-times-team-won-toss-and-won-match.json";

// Find the number of times each team won the toss and also won the match
function numberOfTimesTeamWonTossAndWonMatch() {

  csvToJson(matchesPath).then((matches) => {
    const tossAndMatchWinCounts = {};
    // console.log(matches);
    for (let match of matches) {
      const matchWon = match.winner;
      const tossWon = match.toss_winner;
      if (tossWon === matchWon) {
        if (tossAndMatchWinCounts[tossWon]) {
          tossAndMatchWinCounts[tossWon]++;
        } else {
          tossAndMatchWinCounts[tossWon] = 1;
        }
      }
    }

    fs.writeFileSync(
      outputFilePath,
      JSON.stringify(tossAndMatchWinCounts),
      (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log(
            "number-of-times-team-won-toss-and-won-match.json file is created"
          );
        }
      }
    );
  });
}
console.log(numberOfTimesTeamWonTossAndWonMatch());
