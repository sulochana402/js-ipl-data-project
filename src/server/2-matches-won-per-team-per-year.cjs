const fs = require("fs");
const csvToJson = require("../server.cjs");

const csvFilePath = "../data/matches.csv";
const outputFilePath = "../public/output/2-matches-won-per-team-per-year.json";

// Number of matches won per team per year in IPL.
function matchesWonPerTeamPerYear() {
  csvToJson(csvFilePath).then((matches) => {
    const teamPerYear = {};
    for (let index = 0; index < matches.length; index++) {
      const match = matches[index];
      const year = match.season;
      if (teamPerYear[year]) {
        if (teamPerYear[year][match.winner]) {
          teamPerYear[year][match.winner]++;
        } else {
          teamPerYear[year][match.winner] = 1;
        }
      } else {
        teamPerYear[year] = { [match.winner]: 1 };
      }
    }
    fs.writeFileSync(outputFilePath, JSON.stringify(teamPerYear), (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("matches-won-per-team-per-year.json file is created");
      }
    });
  });
}
console.log(matchesWonPerTeamPerYear());
