const fs = require("fs");
const csvToJson = require("../server.cjs");

const matchesPath = "../data/matches.csv";
const deliveriesPath = "../data/deliveries.csv";
const outputFilePath = "../public/output/3-extra-runs-in-2016.json";

// Extra runs conceded per team in the year 2016
function extraRunsPerTeamIn2016() {
  let matchesData;
  let deliveriesData;

  return csvToJson(matchesPath)
    .then((matches) => {
      matchesData = matches;

      return csvToJson(deliveriesPath);
    })
    .then((deliveries) => {
      deliveriesData = deliveries;

      const extraRunsPerTeam = {};

      for (let index = 0; index < matchesData.length; index++) {
        const match = matchesData[index];
        if (match.season === "2016") {
          const matchId = match.id;

          for (let index = 0; index < deliveriesData.length; index++) {
            const delivery = deliveriesData[index];
            // console.log(deliveries);
            if (delivery.match_id === matchId) {
              const bowlingTeam = delivery.bowling_team;
              const extraRuns = parseInt(delivery.extra_runs);

              if (extraRunsPerTeam[bowlingTeam]) {
                extraRunsPerTeam[bowlingTeam] += extraRuns;
              } else {
                extraRunsPerTeam[bowlingTeam] = extraRuns;
              }
            }
          }
        }
      }
      fs.writeFileSync(
        outputFilePath,
        JSON.stringify(extraRunsPerTeam),
        (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("extra-runs-in-2016.json file is created");
          }
        }
      );
    });
}
console.log(extraRunsPerTeamIn2016());
