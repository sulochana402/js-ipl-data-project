const fs = require("fs");
const csvToJson = require("../server.cjs");

const deliveriesPath = "../data/deliveries.csv";
const outputFilePath = "../public/output/9-bowler-with-best-economy-in-super-overs.json";

// Find the bowler with the best economy in super overs
function bowlerWithBestEconomyInSuperOvers() {
  return csvToJson(deliveriesPath).then((deliveries) => {
    const superOvers = {};
    const bowlersArray = [];
    for (let i = 0; i < deliveries.length; i++) {
      const delivery = deliveries[i];

      // Check if the delivery was in a super over
      if (delivery.is_super_over === "1") {
        const bowler = delivery.bowler;
        const totalRuns = parseInt(delivery.total_runs);

        if (superOvers[bowler]) {
          superOvers[bowler].totalBalls += 1;
          superOvers[bowler].totalRuns += totalRuns;
        } else {
          superOvers[bowler] = {
            totalBalls: 1,
            totalRuns: totalRuns,
          };
        }
      }
    }
    for (const bowler in superOvers) {
      const { totalBalls, totalRuns } = superOvers[bowler];
      const economy = (totalRuns / totalBalls) * 6;

      bowlersArray.push({
        bowler: bowler,
        totalBalls: totalBalls,
        totalRuns: totalRuns,
        economy: economy,
      });
    }
    // Sort the bowlers by economy rate
    bowlersArray.sort((a, b) => a.economy - b.economy);

    const bestBowler = bowlersArray[0];
    fs.writeFileSync(outputFilePath, JSON.stringify(bestBowler), (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(
          "9-bowler-with-best-economy-in-super-over.json file is created"
        );
      }
    });
  });
}
console.log(bowlerWithBestEconomyInSuperOvers());
