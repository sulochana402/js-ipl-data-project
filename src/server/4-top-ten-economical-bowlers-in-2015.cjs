const fs = require("fs");
const csvToJson = require("../server.cjs");

const matchesPath = "../data/matches.csv";
const deliveriesPath = "../data/deliveries.csv";
const outputFilePath = "../public/output/4-top-ten-economaical-bowlers-in-2015.json";

function topTenEconomicalBowlersIn2015() {
  let matchesData;
  let deliveriesData;

  return csvToJson(matchesPath)
    .then((matches) => {
      matchesData = matches;

      return csvToJson(deliveriesPath);
    })
    .then((deliveries) => {
      deliveriesData = deliveries;

      const economyMap = {};

      for (let index = 0; index < matchesData.length; index++) {
        if (matchesData[index].season === "2015") {
          const data = matchesData[index];
          for (let i = 0; i < deliveriesData.length; i++) {
            const delivery = deliveriesData[i];
            if (data.id === delivery.match_id && delivery.bowling_team) {
              if (economyMap[delivery.bowler] === undefined) {
                economyMap[delivery.bowler] = { runs: 0, balls: 0 };
              }
              economyMap[delivery.bowler].runs += Number(delivery.total_runs);
              economyMap[delivery.bowler].balls++;
            }
          }
        }
      }
      // Calculate economy rate for each bowler
      const top10Bowlers = [];
      for (const bowler in economyMap) {
        const { runs, balls } = economyMap[bowler];
        const economyRate = (runs / balls) * 6;
        top10Bowlers.push({ bowler, economyRate });
      }

      // Sort bowlers based on economy rate
      top10Bowlers.sort((a, b) => a.economyRate - b.economyRate);
      const topten = top10Bowlers.splice(0, 10);

      fs.writeFileSync(outputFilePath, JSON.stringify(topten), (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log(
            "top-ten-economical-bowlers-in-2015.json file is created"
          );
        }
      });
    });
}
topTenEconomicalBowlersIn2015();
