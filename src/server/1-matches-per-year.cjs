const fs = require("fs");
const csvToJson = require("../server.cjs");

const csvFilePath = "../data/matches.csv";
const outputFilePath = "../public/output/1-matches-per-year.json";

// Number of matches played per year for all the years in IPL.
function matchesPerYear() {
  csvToJson(csvFilePath).then((matches) => {
    let matchesPerYear = {};

    for (let index = 0; index < matches.length; index++) {
      const match = matches[index];
      const year = match.season;

      if (matchesPerYear[year]) {
        matchesPerYear[year]++;
      } else {
        matchesPerYear[year] = 1;
      }
    }
    fs.writeFileSync(outputFilePath, JSON.stringify(matchesPerYear), (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("matches-per-year.json file is created");
      }
    });
  });
}
console.log(matchesPerYear());
