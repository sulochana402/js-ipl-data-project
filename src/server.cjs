const csv = require("csvtojson");

function csvToJson(path){
    return csv().fromFile(path).then((data) =>{
        return data;
    })
}
module.exports = csvToJson;